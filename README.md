# Quick Sort

Quick Sort implementation in PHP.

## Usage

Download or clone the project:

```git
git clone git@gitlab.com:Tavafi/quick-sort.git
```

Instructs composer to create autoloader:

```composer
composer dump-autoload
```

Configure your web server's document / web root to be the  `public` directory. The `index.php` in this directory serves as the front controller for all HTTP requests entering your application.
The main directory of the application is `app` with predefined structure.

**Or**

You can see algorithm implementation in `./quick-sort.php` file. You can copy it into `web root` directory and run it by: [http://localhost/quick-sort.php](http://localhost/quick-sort.php)